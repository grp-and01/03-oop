import kotlin.math.sqrt

fun main(args: Array<String>) {
    val p1 = Point(3, 4)
    val c = Circle(Point(0,0), 5f)
    println(c.isInCircle(p1))
}

///////////////////////////////////////////////////

/// 1
class Point(var x: Int, var y: Int) {
    override fun toString(): String {
        return "[$x, $y]"
    }

/// 2
    fun move(otherPoint: Point) {
        x += otherPoint.x
        y += otherPoint.y
    }
/// 3
	fun distance(otherPoint: Point): Float {
        var diffX = x - otherPoint.x
        var diffY = y - otherPoint.y
        return sqrt((diffX * diffX + diffY * diffY).toFloat())
    }
}
/// 4
class Circle(val center: Point, val radius: Float) {
    fun isInCircle(p: Point): Boolean {
        return center.distance(p) <= radius
    }
}

